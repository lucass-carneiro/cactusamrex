#ifndef IO_NORM_HXX
#define IO_NORM_HXX

#include <fixmath.hxx>
#include <cctk.h>

namespace CarpetX {

void OutputNorms(const cGH *restrict cctkGH);

}

#endif // #define IO_NORM_HXX
